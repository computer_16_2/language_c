#include <stdio.h>

#define Arr_Length 5

void reverse(int arr[], int n) {
	// 定义一个整型变量i和变量tmp 
	int i,tmp;
	// 循环将第一个元素个第五个元素对调 、 第二个元素个第四个元素对调
	for(i=0;i<n/2;i++) {
		//第一次循环时，i=0,  将arr[0]的值赋值给tmp变量，将arr[n-1-i],n是传递过来的实参的值是5，即将arr[5-1-0]=arr[4]的值，赋值给arr[0]，之后将之前存储的tmp变量的值赋值给arr[4]
		// 这样就实现了数组中第一个元素和最后一个元素的对调 
		
		//第二次循环时，i=1,  将arr[1]的值赋值给tmp变量，将arr[n-1-i],n是传递过来的实参的值是5，即将arr[5-1-1]=arr[3]的值，赋值给arr[0]，之后将之前存储的tmp变量的值赋值给arr[3]
		// 这样就实现了数组中第一个元素和最后一个元素的对调 
		
		tmp = arr[i];
		
		arr[i] = arr[n-1-i];
		
		arr[n-1-i] = tmp;
	}
	
} 

main() {
	// 定义一个长度为Arr_Length的值得整型数组，前5个元素分别为1、2、3、4、5 
	int arr[Arr_Length] = { 1, 2, 3, 4, 5 };
	
	// 调用定义的reverse函数，将arr数组和数组的长度传递给reverse函数 
	reverse(arr, Arr_Length);
	
	// 输出arr函数中的元素 
	int i = 0;
	for(;i<Arr_Length;i++) {
		printf("%4d", arr[i]);
	}
}
