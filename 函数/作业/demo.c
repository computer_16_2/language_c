#include <stdio.h>

main() {
	// 函数原型（函数的声明） 
	void showNum(int x);
	// 整形变量x 
	int x;
	
	printf("请输入一个整数：");
	// 将输入的值赋值给x 
	scanf("%d", &x);
	
	//  调用showNum这个函数，这里的x是 实参 
	showNum(x);
}

// 定义showNum函数，这里的x是形参 ， void表示 这个函数没有返回值 
void showNum(int x) {
	// 定义一个整形的变量i，来控制循环 
	int i;
	// i的初值是形参x的值，循环终止的条件是i<=0 
	for(i=x;i>0;i--) {
		// 输出当前i的值 
		printf("%4d", i);
		// 循环里的语句序列执行完后，执行语句3 i--; 
	}
}
