// 斐波那契数列   Fibonacci
// 1 1 2 3 5 8
#include <stdio.h>

int fib( int n ) {

	if (n <= 2) return 1; 

	else return fib(n-1) + fib(n-2);

}

main() {

	int n,result;

	printf("请输入一个整数：");

	scanf("%d", &n);

	result = fib(n);
	//  传递过去的实参n的值是4
	//  形参n的值就是实参传递过来的值，也是4
	//  fib(4-1) + fib(4-2)   ===  fib(3) + fib(2) 
	//  fib(3) === fib(3-1) + fib(3-2) === fib(2) + fib(1) === 1 + 1 = 2
	//  fib(3) + fib(2) === 2 + 1 = 3

	// 作业：分析当n=5的时候，程序的执行过程

	printf("result = %d\n", result);
}
