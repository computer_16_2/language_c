// 1. 局部变量    ==   在函数内部定义的变量 ，在函数外部不能使用 
// 2. 全局变量    ==   在函数外部定义的变量 ,    作用域范围为从定义的地方开始，到文件结束。 
// 3. 静态局部变量  ==  首先它满足局部变量的定义条件（在函数内部定义），加上了static关键字 

#include <stdio.h>

// 这里的变量y是在函数外部定义的，所以y是全局变量 
int y = 10; 

void printNum() {
	// 这里的x就是局部变量 
	int x = 5;
	printf("x=%d\n", x);
	printf("y=%d\n", y);
} 

main() {
	printNum();
	// 因为x是在printNum这个函数内部定义的，所以在printNum这个函数外面是不能调用的。 
	//	printf("%d", x);
	
	printf("y=%d\n", y);
}
