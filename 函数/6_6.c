#include <stdio.h>

// arr是传递过来的数组，n表示这个数组的长度
int max(int arr[], int n) {

	int x = 5, i, result;

	for(i=0;i<n;i++) {

		result = arr[i] > x ? arr[i] : x;

		printf("较大值为: %d\n", result);

		// 第一次循环的时候，i=0  ，arr[0](3), 3>5? 3:5;
		// 第二次循环的时候，i=1  ，arr[1](4), 4>5? 4:5;
		// 第三次循环的时候，i=2  ，arr[2](5), 5>5? 5:5;
		// 第四次循环的时候，i=3  ，arr[3](6), 6>5? 6:5;
		// 第五次循环的时候，i=4  ，arr[4](7), 7>5? 7:5;
	}

}

main() {

	int arr[] = {3,4,5,6,7};

	max(arr, 5);

}
