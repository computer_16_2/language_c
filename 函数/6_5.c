#include <stdio.h>

int max(int n) {
	int x = 5;
	return n > x ? n : x;
}

main() {

	int arr[] = {3,4,5,6,7};

	int result,i;

	for(i=0;i<5;i++) {

		result = max(arr[i]);

		printf("较大值为: %d\n", result);

		// 第一次循环的时候，i=0  ，传递的值是arr[0](3), 3>5? 3:5;
		// 第二次循环的时候，i=1  ，传递的值是arr[1](4), 4>5? 4:5;
		// 第三次循环的时候，i=2  ，传递的值是arr[2](5), 5>5? 5:5;
		// 第四次循环的时候，i=3  ，传递的值是arr[3](6), 6>5? 6:5;
		// 第五次循环的时候，i=4  ，传递的值是arr[4](7), 7>5? 7:5;
	}

}