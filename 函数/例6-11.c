#include <stdio.h>

int i;

void sub() {
	int a = 0;
	// 这里的变量b就是静态局部变量，它的作用域仍然为sub函数的作用域，但是只初始化一次 
	static int b = 0;
	a++;
	b++;
	printf("%d: a=%d, b=%d\n",i,a,b);
} 

main() {
	// 在main函数中并没有定义i,为什么这里i没有报错？ 
	// 因为这里i是全局变量 
	for (i=1;i<3;i++) {
		sub();
		//  第一次循环的时候，i=1,调用sub函数，  局部变量a=0, 静态局部变量b=0,   a++=1,   b++ = 1，  输出a，b的值
		//  第二次循环的时候，i=2,调用sub函数，  局部变量a=0, 态局部变量b=1,   a++=1,   b++ = 2，  输出a，b的值  
	}
}
