#include <stdio.h>

int show(int x) {
	if (x==2) return 3;
	else return 2;
}

main() {
	
	int a=3,b;
	// 把show(a)的执行结果的返回值作为一个实参，继续传递执行 
	b = show(  show(a)  );
	
	printf("%d",b);
}
