#include <stdio.h>

int max(int x,int y) {
	// x=10, y=5
	int z;    // 声明部分 
	z = x > y ? x : y;    
	// z=10 
	x++;
	// x=11
	y--;
	// y=4;
	return z;
}

main() {
	int a=5,b=10,c;
	printf("请输入两个整数：");
//	scanf("%d%d",&a,&b);
//	c = max(a,b);
	c = max(b, a);
	printf("最大值：%d\n",c);
	printf("a：%d\n",a);
	printf("b：%d\n",b);
}
