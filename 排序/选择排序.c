#include <stdio.h>
main() {
	
	int i,j, num[3] = { 10, 9, 8 }, x, min, y;
	
	// 经过第一次循环，数组变为8， 10， 9 
	// 第二次循环的时候， 10对应的应该是num[1] 
	
	for(j = 0;j<3;j++) {
		x=j;
		min=num[j]; 
		for(i=j+1;i<3;i++) {
			if (num[i] < min) {
				min = num[i];
				x = i;
			}
		}
		// min = 8, x= 1
		if(x != j) {
			y = num[j];
			num[j] = min;
			num[x] = y;
		}	
	}
	
	for(i=0;i<3;i++) {
		
		printf("%4d", num[i]);
	}
	
	
}
