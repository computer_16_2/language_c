#include <stdio.h>
main() {
	int arr[] = {5,9,4,3,8,1,7,10,2,6};
	int i,j,min,sub,temp;
	for(i=0;i<10;i++) {
		// 将循环的第一个元素假设为最小值 
		sub=i;
		min = arr[i];
		
		for(j=i+1;j<10;j++) {
			if(arr[j] < min) {
				min = arr[j];
				sub=j;
			}
		} 
		
		temp = arr[i];
		arr[i] = min;
		arr[sub] = temp;
	}
	
	for(i=9;i>=0;i--) {
		printf("%4d",arr[i]);
	}
}
