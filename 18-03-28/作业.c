#include <stdio.h>

main() {
	float score;
	printf("请输入您的成绩(0~100)：");
	scanf("%f", &score);
	if  (score < 0 || score > 100) {
		printf("输入的成绩有误，请重新输入！");
	}
	else if (score >= 75) {
		printf("优秀");
	}
	else if (score >= 60) {
		printf("及格");
	}
	else {
		printf("不及格");
	} 
} 
