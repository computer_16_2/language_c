#include <stdio.h>

struct student {
	char no[8], name[10];
	int math, english, chinese;
	int total;
	float average;
};

// 定义结构体变量s 
struct student s;

main() {
	printf("\n 输入：学号 姓名 语文 数学 英语 \n");
	// 思考：为什么前面没有&符号，后面有&运算符 
	// 因为no跟name是字符数组，字符数组赋值的时候不用取值运算符 
	scanf("%2s %6s %d %d %d", s.no, s.name, &s.chinese, &s.math, &s.english);
	
	s.total = s.chinese + s.math + s.english;
	
	s.average = s.total/3.0; 
	
	printf("总分：%d\n", s.total);
	printf("平均分：%8.1f\n", s.average);
} 


