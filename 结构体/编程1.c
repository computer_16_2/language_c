#include <stdio.h>
#define N 10

struct student {
	char name[20], number[9];
	int score;
};

main() {
	int fun1(struct student stu[]);
	// stu[N] 是结构体数组 
	// 第一种赋值方法 
	struct student stu[N] = {
	   {"zhang liang", "11040301", 87},
	   {"li hong", "11040302", 92},
	   {"yan feng", "11040303", 83} 
	};
	
	int sub = fun1(stu);

    printf("%d", stu[sub].score);
}

int fun1(struct student stu[]) {
	int i,max,sub;
	max = stu[0].score;
	sub = 0;
	for(i=1;i<N;i++) {
		if (stu[i].score > max) {
			max = stu[i].score;
			sub = i;
		}
	}
	return sub;
}


