#include <stdio.h>

struct student {
	char no[8], name[10], sex[3];
	struct {
		int day, month, year;
	} birth;
};

main() {
	struct student s;
	
	s.birth.year = 2015;
	s.birth.month = 8;
	s.birth.day = 18;
	
	printf("%d-%d-%d", s.birth.year, s.birth.month, s.birth.day);
}
