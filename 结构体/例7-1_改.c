#include <stdio.h>
main() {
	// 定义了一个结构体 
	struct date {
		// 数据项或域 
		int year,month,day;
		// int year;
		// int month;
		// int day; 
	};
	// 定义一个结构体变量today ，初始化 
	struct date today = {
	    2018, 05, 29
		// 2018对应year，05对应month，29对应day，按顺序依次对应	
	};

//	struct date today[20] = {
//		{2018,05,29},
//		{2018,05,30}	
//	};
	
	struct date today2; 
	// 同类型的结构体 ，都是date，可以直接赋值 
	today2 = today;
	
//	printf("输入今天的日期（年月日）：") ;
	// 分别给变量today中 year、month、day赋值 
//	scanf("%d%d%d", &today.year, &today.month, &today.day);
	//  输出变量today中 year、month、day赋值  
	printf("%d年%d月%d日 \n", today2.year, today2.month, today2.day);
//	printf("%d年%d月%d日 \n", today[1].year, today[1].month, today[1].day);
}
