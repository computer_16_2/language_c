#include <stdio.h>
main() {
	int num, i = 1, result = 1;
	printf("输入您的年龄:");
	scanf("%d",&num);
	do {
		result = result * i;
		i++;
		// 第一次循环的时候，i=1 结果=result*1
		// 第二次循环的时候，i=2,结果=result*2
		// 第三次循环的时候，i=3,结果=result*3
		// ...
		// 一直执行到i=num的时候
	}while(i <= num);

	printf("%d", result);
}
