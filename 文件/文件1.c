#include <stdio.h>
// 这个头文件有一个学名（官方名称）：标准输入/输出函数 
main() {
	// fopen   = f = file   fopen  打开 
	// fclose = f   +   close 
	
	FILE *fp;
	// 这个是固定格式。
	// fp = fopen(文件名, 文件使用方式); 
	fp = fopen("1.txt", "a");
	// 到这一步仅仅只是打开了1.txt这个文件，并没有对这个文件进行任何操作 
	// 文件使用方式还有：w(写)、a(追加)、rb（只读）、wb（写）、ab(追加) 
	
	// fputc(字符, 文件指针)  f=file    put      c = char
	// fgetc(文件指针)
	// fgets(串始地址, 字符数 + 1, 文件指针)  f = file    get    s = string
	// fputs(串始地址, 文件指针) 
	
	char a[20];  // 定义了一个含有20个元素的字符数组a 
	fgets(a, 11, fp);   // 将fp文件指针中的内容读取 10个字符，赋值给字符数组a 
	
	char b[10] = "ABCD";   
	fputs(b, fp);   // 向指针文件中输入字符数组b的内容 
	
	puts(a);   // 将一个字符数组输出到控制台	
	
	fclose(fp); 
	
	// 例9-1 
}
