// 输入5个学生的成绩，先计算总分，再计算平均数
#define students 5

#include <stdio.h>
//　void表明定义的函数没有返回值，arrInput叫函数名，int arr[],int n叫做形参 


// 解释名词：解耦、耦合 
void arrInput(float arr[], int n) {
	int i;
	for(i=0;i<n;i++) {			// 2. 
		scanf("%f",&arr[i]);	               // 3. 对数组进行赋值 
	}
} 

float arrSum(float arr[], int n) {
	int i;
	float sum;
	for(i=0;i<n;i++) {
		sum += arr[i];
	}
	return sum;
}

main() {
	float scores[students],sum;         // 1. 定义变量总分和数组 
	// arrInput()代表调用执行我定义的函数，这里的scores和7叫做实参 
	// 调用arrInput()函数，对scores函数进行赋值操作 
	arrInput(scores, students);
	// sum = arrInput()表示将arrInput的执行结果赋值给main函数中的sum
	sum = arrSum(scores, students);
	
	printf("平均分=%.2f", sum/students);   // 5.输出平均分           
} 
