// 从键盘输入一个整数（0-100），如果数值大于等于60
// 则输出“合格”，否则，输出“不合格”
#include <stdio.h>
main() {
	int score;
	printf("请输入0-100之间的整数:"); 
	scanf("%d",&score);
	if (score < 0 || score > 100)  // A && B 、 !A 
		printf("输入的数值有误！");
	else if(score>=60) printf("合格");
	else printf("不合格");
} 
