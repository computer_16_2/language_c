// 利用循环依次输出数字235813
#include <stdio.h>
main() {
	int a = 1, b = 1;
	for(;sum<=9;) {
		sum = a + b;
		printf("%d",sum);
		a = b;// 将b的值赋值给a 
		b = sum;// 将sum的值赋值给b 
		//　第一次循环的时候，sum=1+1=2
		//  a = b = 1
		//  b = sum = 2
		//  第二次循环的时候，sum = a + b = 1 + 2 = 3
		//  a = b = 2
		//  b = sum = 3 
	}
}
