#include <stdio.h>
main() {
	
	int i, count = 0;
	
	float scores[10], ave;   // ave就是 average 
	
	printf("输入10个学生成绩: \n");
	
	for(i = 0;i < 10;i++) {
		
		scanf("%f", &scores[i]);
		// ave的值是什么？ 
		ave += scores[i];
	} 
	
	ave /= 10; 
	
	printf("平均成绩为：%.1f\n", ave);
	
	for(i=0;i<10;i++) {
		
		if (scores[i] < ave) count++;
		
	}
	
	printf("低于平均成绩的人数为：%2d", count);
	
}
