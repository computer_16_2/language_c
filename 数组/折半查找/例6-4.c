#include <stdio.h>
#define N 8
main() {
	int x, r[N] = { -21, -13, -9, -2, 0, 3, 8, 15 };
	int left, right, mid;
	printf("输入要查找的数："); 
	scanf("%d", &x);
	// 如果说我输入的值是0 
	left = 0, right = N - 1;
	// left、 right、mid都是代表的数组的下标 
	// left = 0, right = 8 - 1 = 7 
	while(left <= right) {
		// 如果说left的值小于right的值，就执行循环 
		// 第一次循环的时候，left <= right ==>   0 <= 7成立，执行循环
		// 第二次循环的时候，left = 4， right = 7， 4 <= 7成立 ， 执行循环 
		// 第三次循环的时候，left = 4， right = 4， 4 <= 4 成立，执行循环 
		mid = (left + right) / 2;
		// 第一次循环的时候，mid = (0 + 7) / 2 =  3
		// 第二次循环的时候 ，mid = (4 + 7) / 2 =  5
		// 第三次循环的时候，mid = （4 + 4） / 2 = 4 
		if (x == r[mid]) break;
		// x == r[mid] 相当于 
		// 第一次循环的时候，mid = 3, x == r[mid]   ==>  0 == -2不成立 ， 就不执行if中的语句 break; 
		// 第二次循环的时候 ，mid = 5, x == r[mid]   ==>  0 == 3不成立，就不执行if中的语句break；
		// 第三次循环的时候， mid = 4, x == r[mid]   ==>  0 == 0成立，执行if中的语句break;  跳出while循环 ，不执行循环的剩余语句 
		if (x < r[mid]) {
		// 第一次循环的时候， x < r[mid] ==>  0 < -2不成立，不执行if中的语句，转而执行else中的语句 ,left = 4
		// 第二次循环的时候， x < r[mid]   ==>   0 < 3成立，就执行if中的语句，不执行else中的语句， right = mid - 1 = 5 - 1 = 4  
			right = mid - 1;
		} 
		else { 
			left = mid + 1;
		}
	}
	if(left <= right) {
		// left = 4, right = 4,left <= right  ==>   4 <= 4成立 ，执行if中的语句，不执行else中的语句，输出 “0是第5个元素” 
		printf("%d是第%d个元素\n", x, mid + 1);
	}
	else {
		printf("没有找到%d\n", x);
	}
	
	// 顺序查找是怎么办的
	// 从数组的第一个元素开始比较
	// 一共要找5次才能找到需要找的值 
}