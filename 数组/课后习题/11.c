#include <stdio.h>
main() {
	int a[6][6] = {};
	int i,j;
	
	for(i=0;i<6;i++) {
		for(j=0;j<6;j++) {
			if(i == j) {
				a[i][j] = 1;
			}
			else if (j == (5 - i)) {
				a[i][j] = -1;
			}
		}
	}
	
	for(i=0;i<6;i++) {
		for(j=0;j<6;j++) {
			printf("%4d",a[i][j]);
		}
		printf("\n");
	}
}
